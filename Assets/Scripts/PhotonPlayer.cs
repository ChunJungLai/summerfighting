﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using MoenenVoxel;
using Photon.Realtime;
using UnityEngine.UI;

public class PhotonPlayer : MonoBehaviourPun,IPunObservable
{
    public InputStr Input;
    public struct InputStr
    {
        public float Speed;
        public float RunWalkID;
        public float MovementID;
        public float AttackID;
        public float AttackRandom;
        public float AttackSpeed1;
        public float AttackSpeed2;
        public bool OnGround;
        public bool Attack1;
        public bool Attack2;
        public bool Dash;
    }

    public Text Name;
    public GameObject PlayerInfoUI;
    public Animator Animator;
    public PhotonView MyPhotonView;
    private PlayerBehaviour mPlayerBehaviour;

    private void Awake()
    {
        mPlayerBehaviour = GetComponent<PlayerBehaviour>();
        /*if (!MyPhotonView.IsMine && mPlayerBehaviour != null)
        {
            Destroy(mPlayerBehaviour);
        }*/

        Name.text = "[ " + MyPhotonView.Owner.NickName + " ]";
    }

	// Update is called once per frame
	private void Update ()
    {
        PlayerInfoUI.transform.LookAt(transform.position+Camera.main.transform.rotation*Vector3.forward, Camera.main.transform.rotation * Vector3.up);
    }

    [PunRPC]
    public void RefreshCharacterData()
    {
        Name.text = "[ " + MyPhotonView.Owner.NickName + " ]";
    }

    public void OnPhotonSerializeView(PhotonStream iStream, PhotonMessageInfo info)
    {

        if(iStream.IsWriting )
        {
            iStream.SendNext(Animator.GetFloat("Speed"));
            iStream.SendNext(Animator.GetFloat("RunWalkID"));
            iStream.SendNext(Animator.GetFloat("MovementID"));
            iStream.SendNext(Animator.GetFloat("AttackID"));
            iStream.SendNext(Animator.GetFloat("AttackRandom"));
            iStream.SendNext(Animator.GetFloat("AttackSpeed1"));
            iStream.SendNext(Animator.GetFloat("AttackSpeed2"));
            iStream.SendNext(Animator.GetBool("OnGround"));
            iStream.SendNext(Animator.GetBool("Attack1"));
            iStream.SendNext(Animator.GetBool("Attack2"));
            iStream.SendNext(Animator.GetBool("Dash"));
        }
        else
        {
            Input.Speed = (float)iStream.ReceiveNext();
            Input.RunWalkID = (float)iStream.ReceiveNext();
            Input.MovementID = (float)iStream.ReceiveNext();
            Input.AttackID = (float)iStream.ReceiveNext();
            Input.AttackRandom = (float)iStream.ReceiveNext();
            Input.AttackSpeed1 = (float)iStream.ReceiveNext();
            Input.AttackSpeed2 = (float)iStream.ReceiveNext();
            Input.OnGround = (bool)iStream.ReceiveNext();
            Input.Attack1 = (bool)iStream.ReceiveNext();
            Input.Attack2 = (bool)iStream.ReceiveNext();
            Input.Dash = (bool)iStream.ReceiveNext();
        }

       /* if (MyPhotonView.IsMine)
        {
            Animator.SetFloat("Speed", Input.Speed);
            Animator.SetFloat("RunWalkID", Input.RunWalkID);
            Animator.SetFloat("MovementID", Input.MovementID);
            Animator.SetFloat("AttackID", Input.AttackID);
            Animator.SetFloat("AttackRandom", Input.AttackRandom);
            Animator.SetFloat("AttackSpeed1", Input.AttackSpeed1);
            Animator.SetFloat("AttackSpeed2", Input.AttackSpeed2);
            Animator.SetBool("OnGround", Input.OnGround);
            Animator.SetBool("Attack1", Input.Attack1);
            Animator.SetBool("Attack2", Input.Attack2);
            Animator.SetBool("Dash", Input.Dash);
        }*/
    }
}
