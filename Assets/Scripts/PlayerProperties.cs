﻿using Photon.Pun;
using Photon.Realtime;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using UnityEngine;

public static class PlayerProperties
{

    public static string IS_READY = "IsReady";
    public static string TEAM = "Team";
    public static string TEAM_RED = "RED";
    public static string TEAM_BLUE = "BLUE";

    public static bool CheckTeam(Player iPlayer, string iValue)
    {
        if (!iPlayer.CustomProperties.ContainsKey(TEAM)) { return false; }
        string aPlayerTeam = (string)iPlayer.CustomProperties[TEAM];
        if(aPlayerTeam == iValue)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool CheckProperties(Player iPlayer, string iKey)
    {
        return iPlayer.CustomProperties.ContainsKey(iKey);
    }

}
