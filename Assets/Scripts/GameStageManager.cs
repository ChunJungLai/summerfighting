﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GameStageManager : MonoBehaviour
{
   [Header("Player Prefab")]
    public PhotonPlayer PlayerPrefab;

    [HideInInspector]
    public PhotonPlayer LocalPlayer;
 
	// Use this for initialization

	void Start ()
    {
       PhotonNetwork.Instantiate(PlayerPrefab.gameObject.name, Vector3.zero, Quaternion.identity).GetComponent<PhotonPlayer>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
