﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using UnityEngine.SceneManagement;

public class NetworkConnectionManager : MonoBehaviourPunCallbacks
{

    public enum NetworkPhase
    {
        MASTER_PHASE,
        LOBBY_PHASE,
        ROOM_PHASE
    }

    public NetworkPhase CurrentNetworkPhase = NetworkPhase.MASTER_PHASE;
    public GameObject ConnectPhase;
    public GameObject LobbyPhase;
    public GameObject RoomPhase;

    public GameObject ChangeRedTeamButton;
    public GameObject ChangeBlueTeamButton;
    public GameObject StartButton;
    public GameObject ReadyButton;

    public Button BtnConnectMaster;
    public Text StatusText;
    public Text CurrentPlayerName;

    public InputField CreateRoomName;
    public InputField CustomPlayerName;

    public List<Button> RoomButtons = new List<Button>();
    private List<RoomInfo> m_RoomList = new List<RoomInfo>();

    public List<PlayerInfoUI> RedTeamPlayerInfo = new List<PlayerInfoUI>();
    public List<PlayerInfoUI> BlueTeamPlayerInfo = new List<PlayerInfoUI>();

    public bool TriesToConnectToMaster;
    public bool TriesToConnectToRoom;

    public InputField InputFieldChat;   // set in inspector
    public Text CurrentChannelText;     // set in inspector

    private int RedTeamCount;
    private int BlueTeamCount;
    private string mNickName;
    public PhotonView mPhotonView;
    #region Common System

    //------------------------------------------------------
    //              Start
    //------------------------------------------------------
    void Start()
    {
        SwitchPhase(NetworkPhase.MASTER_PHASE);
        ChangeRedTeamButton.SetActive(false);
        ChangeBlueTeamButton.SetActive(false);
    }

    //------------------------------------------------------
    //              Update
    //------------------------------------------------------
    void Update()
    {
        BtnConnectMaster.gameObject.SetActive(!PhotonNetwork.IsConnected && !TriesToConnectToMaster);
        UpdatePlaerInfo();
        OnEnterSend();
    }
    //------------------------------------------------------
    //              Switch Phase
    //------------------------------------------------------
    void SwitchPhase(NetworkPhase iNetworkPhase)
    {
        ConnectPhase.SetActive(false);
        LobbyPhase.SetActive(false);
        RoomPhase.SetActive(false);

        switch (iNetworkPhase)
        {
            case NetworkPhase.MASTER_PHASE:
                {
                    ConnectPhase.SetActive(true);
                }
                break;
            case NetworkPhase.LOBBY_PHASE:
                {
                    LobbyPhase.SetActive(true);
                }
                break;
            case NetworkPhase.ROOM_PHASE:
                {
                    RoomPhase.SetActive(true);
                }
                break;
        }
    }

    #endregion
    #region Master Connect System
    //------------------------------------------------------
    //              On Click To Connect To Master
    //------------------------------------------------------
    public void OnClickToConnectToMaster()
    {
        PhotonNetwork.OfflineMode = false;
        PhotonNetwork.NickName = CustomPlayerName.text;
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.GameVersion = "v1";

        TriesToConnectToMaster = true;
        if (CustomPlayerName.text == string.Empty)
        {
            StatusText.text = "Please enter a name.";
        }
        else
        {
            StatusText.text = "Connecting...";
            PhotonNetwork.ConnectUsingSettings();

        }
    }
    //------------------------------------------------------
    //              On Disconnected
    //------------------------------------------------------
    public override void OnDisconnected(DisconnectCause iCause)
    {
        base.OnDisconnected(iCause);
        TriesToConnectToMaster = false;
        TriesToConnectToRoom = false;
        Debug.Log(iCause);
    }
    //------------------------------------------------------
    //              On Connected To Master
    //------------------------------------------------------
    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        TriesToConnectToMaster = false;
        StatusText.text = "Connected To Master";
        ConnectToLobby();
    }
    #endregion

    #region Lobby System
    //------------------------------------------------------
    //              On Joined Lobby
    //------------------------------------------------------
    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
        SwitchPhase(NetworkPhase.LOBBY_PHASE);
        StatusText.text = "Joined Lobby";
        CurrentPlayerName.text = PhotonNetwork.LocalPlayer.NickName;
        mNickName = PhotonNetwork.LocalPlayer.NickName;
        for (int aIndex = 0; aIndex < RoomButtons.Count; aIndex++)
        {
            RoomButtons[aIndex].gameObject.SetActive(false);
            RoomButtons[aIndex].GetComponentInChildren<Text>().text = "";
        }
    }
    //------------------------------------------------------
    //              On Room List Update
    //------------------------------------------------------
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        StatusText.text = "RoomList Updated";
        base.OnRoomListUpdate(roomList);
        m_RoomList.Clear();
        m_RoomList = roomList;

        if (m_RoomList.Count > 0)
        {
            for (int aIndex = 0; aIndex < m_RoomList.Count; aIndex++)
            {
                if (m_RoomList[aIndex].PlayerCount > 0)
                {
                    RoomButtons[aIndex].gameObject.SetActive(true);
                    RoomButtons[aIndex].GetComponentInChildren<Text>().text = "Room name: " + m_RoomList[aIndex].Name + " Room Player Count : " + m_RoomList[aIndex].PlayerCount + "/ " + m_RoomList[aIndex].MaxPlayers + "\n";
                    string aRoomName = m_RoomList[aIndex].Name;
                    RoomButtons[aIndex].onClick.AddListener(delegate () { OnClickJoinToRoom(aRoomName); });
                }
            }
        }
    }
    //------------------------------------------------------
    //              Connect To Lobby
    //------------------------------------------------------
    public void ConnectToLobby()
    {
        if (!PhotonNetwork.IsConnected) { return; }

        PhotonNetwork.JoinLobby();
    }

    #endregion

    #region Connect To Room System


    //------------------------------------------------------
    //              On Click Join To Room
    //------------------------------------------------------
    public void OnClickJoinToRoom(string iRoomName)
    {
        if (!PhotonNetwork.IsConnected) { return; }

        TriesToConnectToRoom = true;
        PhotonNetwork.JoinRoom(iRoomName);
    }

    //------------------------------------------------------
    //              On Clickto Create Room
    //------------------------------------------------------
    public void OnClicktoCreateRoom()
    {
        if (!PhotonNetwork.IsConnected) { return; }

        TriesToConnectToRoom = true;
        string aRoomName = CreateRoomName.text;
        if (aRoomName == string.Empty) { aRoomName = "DefaultRoom"; }
        PhotonNetwork.CreateRoom(CreateRoomName.text, new RoomOptions { MaxPlayers = 10 });
    }

    //------------------------------------------------------
    //              On Click Change Team
    //------------------------------------------------------
    public void OnClickChangeTeam()
    {
        if (!PhotonNetwork.IsConnected) { return; }

        Dictionary<int, Player> aPlayers = PhotonNetwork.CurrentRoom.Players;
        if (aPlayers.Count >= 1)
        {
            for (int aIndex = 0; aIndex < aPlayers.Count; aIndex++)
            {
                if (PlayerProperties.CheckProperties(aPlayers[aIndex + 1], PlayerProperties.TEAM) && aPlayers[aIndex + 1].IsLocal)
                {
                    ChangeRedTeamButton.SetActive(false);
                    ChangeBlueTeamButton.SetActive(false);
                    Hashtable aSetPlayerProperties = new Hashtable();
                    if (PlayerProperties.CheckTeam(aPlayers[aIndex + 1], PlayerProperties.TEAM_RED))
                    {
                        ChangeBlueTeamButton.SetActive(false);
                        ChangeRedTeamButton.SetActive(true);

                        aSetPlayerProperties.Add(PlayerProperties.TEAM, PlayerProperties.TEAM_BLUE);
                        if (PhotonNetwork.LocalPlayer.IsLocal)
                        {
                            mPhotonView.RPC("SetPlayerTeam", RpcTarget.All, aPlayers[aIndex + 1], "BLUE");
                        }
                    }
                    else
                    {
                        ChangeRedTeamButton.SetActive(false);
                        ChangeBlueTeamButton.SetActive(true);

                        aSetPlayerProperties.Add(PlayerProperties.TEAM, PlayerProperties.TEAM_BLUE);
                        if (PhotonNetwork.LocalPlayer.IsLocal)
                        {
                            mPhotonView.RPC("SetPlayerTeam", RpcTarget.All, aPlayers[aIndex + 1], "RED");
                        }
                    }
                }
            }
        }
    }

    //------------------------------------------------------
    //              On Joined Room
    //------------------------------------------------------
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        SwitchPhase(NetworkPhase.ROOM_PHASE);
        TriesToConnectToRoom = false;

        Dictionary<int, Player> aPlayers = PhotonNetwork.CurrentRoom.Players;
        if (aPlayers.Count > 1)
        {
            for (int aIndex = 0; aIndex < aPlayers.Count; aIndex++)
            {
                if (!aPlayers[aIndex + 1].IsLocal && aPlayers.Count > 1)
                {
                    if (PlayerProperties.CheckProperties(aPlayers[aIndex + 1], PlayerProperties.TEAM))
                    {
                        if (PlayerProperties.CheckTeam(aPlayers[aIndex + 1], PlayerProperties.TEAM_RED))
                        {
                            RedTeamCount++;
                        }
                        else
                        {
                            BlueTeamCount++;
                        }
                    }
                }
            }

            for (int aIndex = 0; aIndex < aPlayers.Count; aIndex++)
            {
                if (aPlayers[aIndex + 1].IsLocal)
                {
                    SetPlyaerReady(aPlayers[aIndex + 1],false);

                    if (!PlayerProperties.CheckProperties(aPlayers[aIndex + 1], PlayerProperties.TEAM))
                    {
                        if (RedTeamCount > BlueTeamCount)
                        {
                            SetPlayerTeam(aPlayers[aIndex + 1], PlayerProperties.TEAM_BLUE);
                        }
                        else if (BlueTeamCount >= RedTeamCount)
                        {
                            SetPlayerTeam(aPlayers[aIndex + 1], PlayerProperties.TEAM_RED);
                        }
                    }
                }
            }
        }
        else if (aPlayers.Count == 1)
        {
            SetPlyaerReady(PhotonNetwork.LocalPlayer, true);
            SetPlayerTeam(PhotonNetwork.LocalPlayer, PlayerProperties.TEAM_RED);
            RedTeamCount++;
        }

        UpdatePlaerInfo();
       
    }
    //------------------------------------------------------
    //              On Join Random Failed
    //------------------------------------------------------
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        base.OnJoinRandomFailed(returnCode, message);
        PhotonNetwork.CreateRoom("DefaultRoom", new RoomOptions { MaxPlayers = 20 });
    }

    //------------------------------------------------------
    //              On Create Room Failed
    //------------------------------------------------------
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);
        StatusText.text = message;
        TriesToConnectToRoom = false;
    }

    //------------------------------------------------------
    //              On Player Entered Room
    //------------------------------------------------------
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        UpdatePlaerInfo();
    }

    //------------------------------------------------------
    //              On Player Left Room
    //------------------------------------------------------
    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        PhotonNetwork.JoinLobby();
    }

    //------------------------------------------------------
    //              On Player Left Room
    //------------------------------------------------------
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
        UpdatePlaerInfo();
    }

    //------------------------------------------------------
    //              On Master Client Switched
    //------------------------------------------------------
    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        base.OnMasterClientSwitched(newMasterClient);
    }

    //------------------------------------------------------
    //              Update Plaer Info
    //------------------------------------------------------
    private void UpdatePlaerInfo()
    {
        if (!PhotonNetwork.IsConnected) { return; }
        if (!PhotonNetwork.InRoom) { return; }

        for (int aIndex = 0; aIndex < 5; aIndex++)
        {
            RedTeamPlayerInfo[aIndex].gameObject.SetActive(false);
            RedTeamPlayerInfo[aIndex].SetMaster(false);
            BlueTeamPlayerInfo[aIndex].gameObject.SetActive(false);
            BlueTeamPlayerInfo[aIndex].SetMaster(false);
        }
        RedTeamCount = 0;
        BlueTeamCount = 0;
        StatusText.text = string.Empty;

        Dictionary<int, Player> aPlayers = PhotonNetwork.CurrentRoom.Players;
        if (aPlayers.Count >= 1)
        {
            for (int aIndex = 0; aIndex < aPlayers.Count; aIndex++)
            {
                if (PlayerProperties.CheckTeam(aPlayers[aIndex + 1], PlayerProperties.TEAM_RED))
                {
                    RedTeamPlayerInfo[aIndex].SetMaster(aPlayers[aIndex + 1].IsMasterClient);
                    RedTeamPlayerInfo[RedTeamCount].gameObject.SetActive(true);
                    RedTeamPlayerInfo[RedTeamCount].SetNickname(aPlayers[aIndex + 1].NickName);
                    if (aPlayers[aIndex + 1].IsLocal)
                    {
                        RedTeamPlayerInfo[RedTeamCount].SetReady((bool)aPlayers[aIndex + 1].CustomProperties[PlayerProperties.IS_READY]);
                    }
                    RedTeamCount++;
                }
                else
                {
                    BlueTeamPlayerInfo[aIndex].SetMaster(aPlayers[aIndex + 1].IsMasterClient);
                    BlueTeamPlayerInfo[BlueTeamCount].gameObject.SetActive(true);
                    BlueTeamPlayerInfo[BlueTeamCount].SetNickname(aPlayers[aIndex + 1].NickName);
                    if (aPlayers[aIndex + 1].IsLocal)
                    {
                        BlueTeamPlayerInfo[BlueTeamCount].SetReady((bool)aPlayers[aIndex + 1].CustomProperties[PlayerProperties.IS_READY]);
                    }
                    BlueTeamCount++;
                }

                if (aPlayers[aIndex + 1].IsLocal)
                {
                    if (PlayerProperties.CheckTeam(aPlayers[aIndex + 1], PlayerProperties.TEAM_RED))
                    {
                        ChangeBlueTeamButton.SetActive(true);
                        aPlayers[aIndex + 1].CustomProperties[PlayerProperties.TEAM] = PlayerProperties.TEAM_RED;
                    }
                    else
                    {
                        ChangeRedTeamButton.SetActive(true);
                        aPlayers[aIndex + 1].CustomProperties[PlayerProperties.TEAM] = PlayerProperties.TEAM_BLUE;
                    }
                }

                StatusText.text += aPlayers[aIndex + 1].NickName + "  |  " + aPlayers[aIndex + 1].CustomProperties[PlayerProperties.IS_READY] + "  |  " + aPlayers[aIndex + 1].CustomProperties[PlayerProperties.TEAM] + "\n";
            }
        }

        StartButton.SetActive(PhotonNetwork.LocalPlayer.IsMasterClient);
        ReadyButton.SetActive(!PhotonNetwork.LocalPlayer.IsMasterClient);

    }

    #endregion


    //------------------------------------------------------
    //              On Enter Send
    //------------------------------------------------------
    public void OnEnterSend()
    {
        if (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter))
        {
            OnClickSend();
        }
    }

    //------------------------------------------------------
    //              On Click Send
    //------------------------------------------------------
    public void OnClickSend()
    {
        if (this.InputFieldChat != null && this.InputFieldChat.text!= string.Empty)
        {
            mPhotonView.RPC("SendChatMessage", RpcTarget.All, mNickName + " : " + InputFieldChat.text + "\n");
            this.InputFieldChat.text = string.Empty;
        }
    }
    
    //------------------------------------------------------
    //              On Click Ready
    //------------------------------------------------------
    public void OnClickReady()
    {
        if (PhotonNetwork.LocalPlayer.IsLocal)
        {
            bool aReady = (bool)PhotonNetwork.LocalPlayer.CustomProperties[PlayerProperties.IS_READY];
            mPhotonView.RPC("SetPlyaerReady", RpcTarget.All, PhotonNetwork.LocalPlayer, !aReady);
        }
    }

    public void OnClickStart()
    {
        if (PhotonNetwork.LocalPlayer.IsLocal)
        {
            mPhotonView.RPC("GotoGameStage", RpcTarget.All);
        }
    }

    //------------------------------------------------------
    //              On Click Ready
    //------------------------------------------------------
    public void OnClickLeftRoom()
    {
        if(PhotonNetwork.IsMasterClient && PhotonNetwork.CurrentRoom.PlayerCount>1)
        {
            Dictionary<int, Player> aPlayers = PhotonNetwork.CurrentRoom.Players;
            List<Player> aOtherPlayers = new List<Player>();
            for (int aIndex = 0; aIndex < aPlayers.Count; aIndex++)
            {
                if (!aPlayers[aIndex + 1].IsMasterClient)
                {
                    aOtherPlayers.Add(aPlayers[aIndex + 1]);
                }
            }
            PhotonNetwork.SetMasterClient(aOtherPlayers[0]);
        }
        PhotonNetwork.LeaveRoom();
    }

    //------------------------------------------------------
    //            PunRPC  Send Chat Message
    //------------------------------------------------------
    [PunRPC]
    public void SendChatMessage(string inputLine)
    {
        if (string.IsNullOrEmpty(inputLine) || inputLine == string.Empty)
        {
            return;
        }

        CurrentChannelText.text += inputLine;
    }

    //------------------------------------------------------
    //           PunRPC Set Player Team
    //------------------------------------------------------
    [PunRPC]
    public void SetPlayerTeam(Player iPlayer,string iValue)
    {
        Hashtable SetPlayerProperties = new Hashtable();
        SetPlayerProperties.Add(PlayerProperties.TEAM, iValue);
        iPlayer.SetCustomProperties(SetPlayerProperties);
    }

    //------------------------------------------------------
    //         PunRPC   Set Plyaer Ready
    //------------------------------------------------------
    [PunRPC]
    public void SetPlyaerReady(Player iPlayer, bool iValue)
    {
        Hashtable SetPlayerProperties = new Hashtable();
        SetPlayerProperties.Add(PlayerProperties.IS_READY, iValue);
        iPlayer.SetCustomProperties(SetPlayerProperties);
    }

    [PunRPC]
    public void GotoGameStage()
    {
        SceneManager.LoadScene("GameStage");
    }
}
